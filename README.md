# EventsApp

This is an API to manage Events. Events are an entity that is assocciated to a *creator_id* mainly, and could have many participants (Ids). Since it's developed in Elixir/OTP Events when created and at the moment they started are isolated processes (erlang virtual machine processes not system processes) with their own PID. This will allow us to have millions of isolated events that can be tracked by PID and will never overlap each other. When any information needs to be passed to the system this will send/broadcast a message that will handleded as expected by the system. 

## Use

First of all it's necessary to be installed:

 . Elixir
 . Erlang/OTP

It's as simple as running `berw install elixir` in MacOS. [For other systems check here](https://elixir-lang.org/install.html)

# Database

This project uses PostgreSQL. Commands to start up:

 - `mix ecto.create`
 - `mix ecto.migrate` 

Then to start the API just run: `MIX_ENV=dev iex -S mix run` 

*NOTE:* At `/config` folder there is a config file as per environment, if you want to change the port it uses, edit the file and change it.

## TODO: Run in docker

## TODO: Docu

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/events_app](https://hexdocs.pm/events_app).
