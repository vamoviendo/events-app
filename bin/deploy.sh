#!/bin/bash -ex

BRANCH=${1:-master};
VERSION=$2

mix edeliver build release --branch=$BRANCH --verbose
mix edeliver deploy release to production --start-deploy --verbose
mix edeliver migrate production up --verbose
