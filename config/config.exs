# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

import_config "#{Mix.env()}.exs"

# This configuration is loaded before any dependency and is restricted
# to this project. If another project depends on this project, this
# file won't be loaded nor affect the parent project. For this reason,
# if you want to provide default values for your application for
# 3rd-party users, it should be done in your "mix.exs" file.

# You can configure your application as:
#
#     config :events_app, key: :value
#
# and access this configuration in your application as:
#
#     Application.get_env(:events_app, :key)
#
# You can also configure a 3rd-party app:
#
#     config :logger, level: :info
#

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env()}.exs"

config :events_app, EventsApp.Endpoint,
  http: [
    protocol_options: [max_request_line_length: 8192, max_header_value_length: 8192],
    transport_options: [max_connections: 16_384]
  ],
  secret_key_base: "ZlXK4P1qm+H6U8fQX+VL/fP6XVVazsrI"

config :events_app,
  ecto_repos: [EventsApp.Repo]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: :all

config :postgrex, :json_library, Jason

config :geo_postgis,
  json_library: Jason
