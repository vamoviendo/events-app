use Mix.Config

config :events_app, EventsApp.Repo,
  username: "postgres",
  password: "postgres",
  database: "events_app_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox,
  extensions: [{Geo.PostGIS.Extension, library: Geo}],
  types: EventsApp.PostgresTypes,
  migration_lock: nil

config :events_app, port: 4000

config :bitbucket_pipelines_test, BitbucketPipelinesTest.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool: Ecto.Adapters.SQL.Sandbox,
  url: "postgres://postgres:pipelines-test@localhost/test-db"
