use Mix.Config

config :events_app, EventsApp.Endpoint, url: [scheme: "https", host: "localhost", port: 443]

config :events_app, redirect_url: System.get_env("REDIRECT_URL")

config :events_app, EventsApp.Repo,
  username: "postgres",
  password: "postgres",
  database: "events_app",
  hostname: "localhost",
  pool_size: 20,
  types: EventsApp.PostgresTypes,
  extensions: [{Geo.PostGIS.Extension, library: Geo}]

config :events_app, port: 4000

config :logger,
  backends: [:console, {LoggerFileBackend, :error_log}],
  format: "$time $metadata[$level] $message\n",
  metadata: :all

config :logger, :error_log,
  path: "/tmp/events_app.log",
  level: :debug
