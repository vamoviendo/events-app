use Mix.Config

config :events_app, port: 4000

config :events_app, EventsApp.Endpoint,
  load_from_system_env: true,
  url: [scheme: "https", host: "localhost", port: 443],
  server: true,
  code_reloader: false

config :events_app, redirect_url: System.get_env("REDIRECT_URL")

config :events_app, EventsApp.Repo,
  database: "",
  url: System.get_env("DATABASE_URL"),
  pool_size: 5,
  migration_lock: nil,
  extensions: [{Geo.PostGIS.Extension, library: Geo}],
  types: EventsApp.PostgresTypes

config :logger,
  backends: [:console, {LoggerFileBackend, :error_log}],
  format: "[$level] $message\n"

config :logger, :error_log,
  path: "/tmp/events_app.log",
  level: :info
