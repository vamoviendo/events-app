defmodule EventsApp.Repo.Migrations.AddAddressField do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :address, :string
    end
  end
end
