defmodule EventsApp.Repo.Migrations.AddIndexToEvents do
  use Ecto.Migration

  def change do
    create index("events", [:creator_id])
  end
end
