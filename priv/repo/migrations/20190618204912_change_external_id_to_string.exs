defmodule EventsApp.Repo.Migrations.ChangeExternalIdToString do
  use Ecto.Migration

  def change do
    alter table(:events) do
      modify :participants, :string
    end
  end
end
