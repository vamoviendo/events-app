defmodule EventsApp.Repo.Migrations.ModifyCreatorIdToBigint do
  use Ecto.Migration

  def change do
    alter table(:events) do
      modify :creator_id, :bigint
    end
  end
end
