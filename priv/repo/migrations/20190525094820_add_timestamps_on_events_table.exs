defmodule EventsApp.Repo.Migrations.AddTimestampsOnEventsTable do
  use Ecto.Migration

  def change do
    alter table(:events) do
      timestamps()
    end
  end
end
