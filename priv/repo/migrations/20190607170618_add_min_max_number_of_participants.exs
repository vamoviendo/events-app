defmodule EventsApp.Repo.Migrations.AddMinMaxNumberOfParticipants do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :min_num_of_participants_to_start, :integer, default: 0
      add :max_num_of_participants, :integer, default: 0
    end
  end
end
