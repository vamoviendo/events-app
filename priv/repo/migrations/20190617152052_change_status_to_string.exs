defmodule EventsApp.Repo.Migrations.ChangeStatusToString do
  use Ecto.Migration

  def change do
    alter table(:events) do
      modify :status, :string
    end
  end
end
