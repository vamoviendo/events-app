defmodule EventsApp.Repo.Migrations.ChangeParticipantsToArrayOfBigints do
  use Ecto.Migration

  def change do
    alter table(:events) do
      modify :participants, {:array, :bigint}
    end
  end
end
