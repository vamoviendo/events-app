defmodule EventsApp.Repo.Migrations.AddDeleteFields do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :deleted, :boolean, default: false
      add :deleted_at, :utc_datetime
    end
  end
end
