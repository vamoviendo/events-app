defmodule EventsApp.Repo.Migrations.AddGeometryField do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :geom, :geometry
    end
  end
end
