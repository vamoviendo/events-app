defmodule EventsApp.Repo.Migrations.RevertParticipantsToString do
  use Ecto.Migration

  def change do
      execute """
          alter table events alter column participants type integer[] using cast (participants as integer[])
         """
  end
end
