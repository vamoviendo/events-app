defmodule EventsApp.Repo.Migrations.MigrateFieldsToString do
  use Ecto.Migration

  def change do
    alter table(:events) do
      modify :creator_id, :string
      modify :participants, {:array, :string}
    end
  end
end
