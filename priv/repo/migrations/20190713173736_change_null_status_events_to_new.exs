defmodule EventsApp.Repo.Migrations.ChangeNullStatusEventsToNew do
  use Ecto.Migration
  import Ecto.Query, warn: false

  def up do
    from(e in EventsApp.Model.Event,
      update: [set: [status: "new"]],
      where: is_nil(e.status))
    |> EventsApp.Repo.update_all([])
  end
end
