defmodule EventsApp.Repo.Migrations.TimestampsInMicroseconds do
  use Ecto.Migration

  def change do
    alter table(:events) do
      modify :inserted_at, :utc_datetime_usec
      modify :updated_at, :utc_datetime_usec
    end
  end
end
