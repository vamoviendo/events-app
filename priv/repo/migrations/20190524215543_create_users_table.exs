defmodule EventsApp.Repo.Migrations.CreateUsersTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :nickname, :string
      add :email, :string
      add :first_name, :string
      add :last_name, :string
      add :provider, :string
      add :id_from_provider, :integer
    end

    create unique_index(:users, [:id_from_provider])
  end
end
