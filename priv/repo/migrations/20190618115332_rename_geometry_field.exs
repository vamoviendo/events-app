defmodule EventsApp.Repo.Migrations.RenameGeometryField do
  use Ecto.Migration

  def change do
    rename table("events"), :geom, to: :coordinates
  end
end
