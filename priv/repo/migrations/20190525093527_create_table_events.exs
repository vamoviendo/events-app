defmodule EventsApp.Repo.Migrations.CreateTableEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :name, :string
      add :creator_id, :integer
      add :starts_at, :utc_datetime
      add :finishes_at, :utc_datetime
      add :type, :string
      add :description, :string
      add :participants, {:array, :integer}
      add :status, :integer
      add :comments, {:array, :string}
    end
  end
end
