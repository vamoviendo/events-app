defmodule EventsApp.MixProject do
  use Mix.Project

  def project do
    [
      app: :events_app,
      version: "0.2.2",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases(),
      elixirc_paths: elixirc_paths(Mix.env()),
      elixirc_options: [
        warnings_as_errors: halt_on_warnings?(Mix.env())
      ],
      aliases: aliases(),
      dialyzer: [
        plt_file: {:no_warn, "priv/plts/dialyzer.plt"},
        ignore_warnings: ".dialyzer_ignore.exs"
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :scrivener_ecto],
      mod: {EventsApp.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug, "~> 1.6"},
      {:cowboy, "~> 2.4"},
      {:plug_cowboy, "~> 2.0"},
      {:ecto, "~> 3.0", override: true},
      {:postgrex, "~> 0.14"},
      {:ecto_sql, "~> 3.0"},
      {:timex, "~> 3.1"},
      {:scrivener_ecto, "~> 2.0"},
      {:credo, "~> 1.0.0", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0.0-rc.6", only: [:dev, :test], runtime: false},
      {:sobelow, "~> 0.7", only: [:dev, :test], runtime: false},
      {:jason, "~> 1.1"},
      {:geo_postgis, "~> 3.1"},
      {:edeliver, "~> 1.6.0"},
      {:distillery, "~> 2.0.0"},
      {:inquisitor, "~> 0.5"}
    ]
  end

  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.migrate", "test"],
      "test.local": ["ecto.create --quiet", "ecto.migrate", "test"],
      quality: [
        "format",
        "credo --strict",
        "sobelow --verbose --router lib/events_app/controllers/router.ex",
        "dialyzer",
        "test"
      ],
      "quality.ci": [
        "test",
        "format --check-formatted",
        "credo --strict",
        "sobelow --router lib/events_app/controllers/router.ex --exit",
        "dialyzer --halt-exit-status"
      ]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp halt_on_warnings?(:test), do: false
  defp halt_on_warnings?(:dev), do: false
  defp halt_on_warnings?(_), do: false
end
