defmodule EventsApp.Query.EventsFilters do
  defmacro __using__(_opts) do
    quote do
      use Inquisitor

      def build_query(query, "id", id, _conn) do
        Ecto.Query.where(query, [p], p.id == ^id)
      end

      def build_query(query, "creator_id", creator_id, _conn) do
        Ecto.Query.where(query, [p], p.creator_id == ^creator_id)
      end

      def build_query(query, "deleted", bool, _conn) do
        Ecto.Query.where(query, [p], p.deleted == ^bool)
      end

      def build_query(query, "name", str, _conn) do
        like_name = "%#{str}%"
        Ecto.Query.where(query, [p], like(p.name, ^like_name))
      end

      def build_query(query, "statuses", str, _conn) do
        statuses = String.split(str, ",")
        Ecto.Query.where(query, [p], p.status in ^statuses)
      end

      def build_query(query, "status", str, _conn) do
        Ecto.Query.where(query, [p], p.status == ^str)
      end

      def build_query(query, "exclude_creator_ids", ids, _conn) do
        id_list = String.split(ids, ",")
        Ecto.Query.where(query, [p], p.creator_id not in ^id_list)
      end

      def build_query(query, "published", bool, _conn) do
        published_status = ["created", "waiting_for_participants", "ready_to_start"]

        case bool do
          "true" ->
            Ecto.Query.where(query, [p], p.status in ^published_status)

          "false" ->
            Ecto.Query.where(query, [p], p.status not in ^published_status)
        end
      end

      # DATES
      # starts_at
      def build_query(query, "starts_from", param_date, _conn) do
        {_, date_time} = cast_datetime(param_date)
        Ecto.Query.where(query, [p], p.starts_at <= ^date_time)
      end

      def build_query(query, "starts_to", param_date, _conn) do
        {_, date_time} = cast_datetime(param_date)
        Ecto.Query.where(query, [p], p.starts_at >= ^date_time)
      end

      # finishes_at
      def build_query(query, "finishes_from", param_date, _conn) do
        {_, date_time} = cast_datetime(param_date)
        Ecto.Query.where(query, [p], p.finishes_at <= ^date_time)
      end

      def build_query(query, "finishes_to", param_date, _conn) do
        {_, date_time} = cast_datetime(param_date)
        Ecto.Query.where(query, [p], p.finishes_at >= ^date_time)
      end

      def build_query(query, "inserted_at", param_date, _conn) do
        {_, date_time} = cast_datetime(param_date)
        Ecto.Query.where(query, [p], p.inserted_at >= ^param_date)
      end

      # TODO Improve this function and define format
      # to do some test this format would work
      # 2015-01-23T23:50:07Z
      defp cast_datetime(string_date) do
        {status, value} = Timex.parse(string_date, "{ISO:Extended}")
        {status, value}
      end
    end
  end
end
