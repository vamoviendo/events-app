defmodule EventsApp.Model.Event do
  use Ecto.Schema
  import Ecto.Changeset

  @statuses [
    "new",
    "created",
    "waiting",
    "ready",
    "closed",
    "started",
    "on_hold",
    "cancelled"
  ]

  @show_fields [
    :name,
    :creator_id,
    :starts_at,
    :finishes_at,
    :type,
    :description,
    :participants,
    :status,
    :comments,
    :min_num_of_participants_to_start,
    :max_num_of_participants,
    :address,
    :deleted,
    :deleted_at,
    :inserted_at
  ]

  @derive {Jason.Encoder, only: @show_fields}
  schema "events" do
    field(:name, :string)
    field(:creator_id, :string)
    field(:starts_at, :utc_datetime)
    field(:finishes_at, :utc_datetime)
    field(:type, :string)
    field(:description, :string)
    field(:participants, {:array, :string})
    field(:status, :string)
    field(:comments, {:array, :string})
    field(:min_num_of_participants_to_start, :integer, default: 0)
    field(:max_num_of_participants, :integer, default: 0)
    field(:coordinates, Geo.PostGIS.Geometry)
    field(:address, :string)
    field(:deleted, :boolean)
    field(:deleted_at, :utc_datetime)

    timestamps(type: :utc_datetime_usec)
  end

  def encode_model(event) do
    case Map.get(event, :coordinates) do
      %Geo.Point{} = geom ->
        lat = Kernel.elem(geom.coordinates, 0)
        lon = Kernel.elem(geom.coordinates, 1)

        %{event | coordinates: %{lat: lat, lon: lon}}

      _ ->
        event
    end
  end

  defimpl Jason.Encoder, for: EventsApp.Model.Event do
    def encode(event, options) do
      event = EventsApp.Model.Event.encode_model(event)

      map =
        event
        |> Map.from_struct()

      Jason.Encoder.Map.encode(
        Map.take(map, [
          :id,
          :name,
          :creator_id,
          :starts_at,
          :finishes_at,
          :type,
          :description,
          :participants,
          :status,
          :comments,
          :min_num_of_participants_to_start,
          :max_num_of_participants,
          :coordinates,
          :address,
          :deleted,
          :deleted_at,
          :inserted_at
        ]),
        options
      )
    end
  end

  @doc false
  def changeset(event, attrs) do
    attrs = key_to_atom(attrs)

    event
    |> cast(attrs, [
      :name,
      :creator_id,
      :starts_at,
      :finishes_at,
      :type,
      :description,
      :participants,
      :status,
      :comments,
      :min_num_of_participants_to_start,
      :max_num_of_participants,
      :coordinates,
      :address,
      :deleted,
      :deleted_at,
      :inserted_at
    ])
    |> validate_required([:name, :creator_id])
    |> validate_inclusion(:status, @statuses, message: "The given status is not allowed")
    |> validate_event_not_deleted()
  end

  def validate_event_not_deleted(changeset) do
    if changeset.data.deleted do
      add_error(changeset, :error, "Event can not be modified")
    else
      changeset
    end
  end

  def key_to_atom(map) do
    Enum.reduce(map, %{}, fn
      {key, value}, acc when is_atom(key) -> Map.put(acc, key, value)
      # String.to_existing_atom saves us from overloading the VM by
      # creating too many atoms. It'll always succeed because all the fields
      # in the database already exist as atoms at runtime.
      {key, value}, acc when is_binary(key) -> Map.put(acc, String.to_existing_atom(key), value)
    end)
  end
end
