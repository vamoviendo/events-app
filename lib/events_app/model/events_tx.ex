# Reference #1: to_existing_atom will throw exception the first time
# if atom do not exist yet

defmodule EventsApp.Query.Events do
  import Ecto.Query, warn: false
  use EventsApp.Query.EventsFilters

  alias EventsApp.Model.Event
  alias EventsApp.Repo

  # This :check_video_owner is supposed to be a defp fun defined
  # bellow. Useful to pre-call a validation for given action
  # plug :check_video_owner when action in [:delete]

  @doc """
  Creates a new event.

  ## Examples

      iex> create_event(%{field: value})
      {:ok, %Event{}}

      iex> create_event(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_event(attrs \\ %{}) do
    attrs = Map.put(attrs, "status", "new")

    if Map.has_key?(attrs, "coordinates") do
      attrs_with_coordinates = Map.put(attrs, "coordinates", parse_coordinates(attrs))

      %Event{}
      |> Event.changeset(attrs_with_coordinates)
      |> Repo.insert()
    else
      %Event{}
      |> Event.changeset(attrs)
      |> Repo.insert()
    end
  rescue
    _e in [Postgrex.Error, MatchError] -> {:error, %{error: "Event can not be created"}}
  end

  def add_participant(%{"id" => id, "participant_id" => participant})
      when id != "" and participant != "" do
    id = id |> String.to_integer()
    event = Repo.get!(Event, id)

    case event.participants do
      nil ->
        update_changeset(event, %{participants: [participant]})

      [_ | _] ->
        if participant in event.participants do
          update_changeset(event, %{participants: event.participants})
        else
          update_changeset(event, %{participants: event.participants ++ [participant]})
        end

      [] ->
        update_changeset(event, %{participants: [participant]})
    end
  rescue
    _e in Ecto.NoResultsError -> {:error, %{error: "event not found"}}
    _e in ArgumentError -> {:error, %{error: "error parsing the given parameters"}}
  end

  def add_participant(_) do
    {:error, nil}
  end

  def list_all_events do
    Event
    |> build_query(nil, %{"deleted" => false})
    |> Repo.paginate()
  end

  def get_events_by(query_params \\ %{}) do
    Event
    |> build_query(nil, Map.put(query_params, "deleted", false))
    |> Repo.paginate()
  end

  def get_all_participants(id) when id != "" do
    id = id |> String.to_integer()

    try do
      event = Repo.get!(Event, id)
      event.participants
    rescue
      Ecto.NoResultsError -> {:error, %{error: "event not found"}}
    end
  end

  def remove_participant(id, participant_id)
      when id != "" and participant_id != "" do
    id = id |> String.to_integer()

    try do
      event = Repo.get!(Event, id)

      update_changeset(
        event,
        %{participants: List.delete(event.participants, participant_id)}
      )
    rescue
      _e in Ecto.NoResultsError -> {:error, %{error: "event not found"}}
      _e in Ecto.ChangeError -> {:error, %{error: "event could not be modified"}}
    end
  end

  def update_event(id, attrs)
      when id != "" and is_map(attrs) do
    id = id |> String.to_integer()
    event = Repo.get!(Event, id)

    case event do
      %EventsApp.Model.Event{} = event ->
        if Map.get(attrs, "coordinates") do
          update_changeset(event, %{attrs | "coordinates" => parse_coordinates(attrs)})
        else
          update_changeset(event, attrs)
        end
    end
  rescue
    _e in Ecto.NoResultsError -> {:error, %{error: "event not found"}}
    _e in ArgumentError -> {:error, %{error: "error parsing the given parameters"}}
  end

  def delete_event(id) do
    attrs = %{
      deleted: true,
      deleted_at: Timex.now(),
      status: "cancelled"
    }

    Event
    |> Repo.get(id)
    |> update_changeset(attrs)
  end

  defp update_changeset(event, attrs) do
    event
    |> Event.changeset(attrs)
    |> Repo.update!()
  rescue
    e in Ecto.InvalidChangesetError -> e.changeset
    e in [Postgrex.Error, MatchError] -> {:error, %{error: e.changeset.errors}}
    _e in BadMapError -> {:error, %{error: "some parameters can not be parsed"}}
  end

  defp parse_coordinates(attrs) do
    %{"lat" => lat, "lon" => lon} =
      attrs
      |> Map.get("coordinates")

    %Geo.Point{coordinates: {lat, lon}, srid: 4326}
  rescue
    _e in MatchError -> {:error}
  end
end
