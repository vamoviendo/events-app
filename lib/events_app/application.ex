defmodule EventsApp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  Code.compiler_options(ignore_module_conflict: true)
  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      {EventsApp.Repo, []},
      # Starts a worker by calling: EventsApp.Worker.start_link(arg)
      # {EventsApp.Worker, arg},
      EventsApp.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: EventsApp.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
