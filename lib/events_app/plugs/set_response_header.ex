defmodule EventsApp.Plugs.SetResponseHeader do
  def init(_params) do
  end

  def call(conn, _params) do
    Plug.Conn.put_resp_header(conn, "content-type", "application/json")
  end
end
