# defimpl Jason.Encoder, for: Any do
#   def encode(%{__struct__: _} = struct, opts) do
#     require IEx; IEx.pry()
#     struct
#     |> Map.from_struct()
#     |> sanitize_map
#     |> geo_location_as_plain_coordinates()
#     |> Jason.Encode.map(opts)
#   end

#   def encode({:error, %{error: _} = errors}, opts) do
#     Jason.Encode.map(errors, opts)
#   end

#   defp sanitize_map(map) do
#     Map.drop(map, [:__meta__, :__struct__])
#   end

#   defp geo_location_as_plain_coordinates(map) do
#     case Map.get(map, :coordinates) do
#       %Geo.Point{} = geom ->
#         lat = Kernel.elem(geom.coordinates, 0)
#         lon = Kernel.elem(geom.coordinates, 1)

#         %{map | coordinates: %{lat: lat, lon: lon}}

#       _ ->
#         map
#     end
#   end
# end
