defmodule EventsApp.Endpoint do
  use Plug.Router
  use Plug.ErrorHandler
  plug(Plug.SSL, rewrite_on: [:x_forwarded_proto])

  if Mix.env() == :dev do
    use Plug.Debugger, otp_app: :events_app
  end

  plug(Plug.RequestId)

  forward("/events", to: EventsApp.Router)

  plug(:match)

  plug(Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason
  )

  plug(:dispatch)

  match _ do
    send_resp(conn, 404, "Requested page not found!")
  end

  def handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stack}) do
    send_resp(conn, 500, Jason.encode!(%{error: "Unexpected error"}))
  end

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]}
    }
  end

  require Logger

  def start_link(_opts) do
    {:ok, result} = Application.fetch_env(:events_app, __MODULE__)
    port = Application.fetch_env!(:events_app, :port)

    Logger.info("Starting server with options")

    Plug.Cowboy.http(__MODULE__, [], result[:http] ++ [port: port])
  end
end
