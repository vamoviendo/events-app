defmodule EventsApp.Router do
  use Plug.Router
  alias EventsApp.Query.Events
  plug(EventsApp.Plugs.SetResponseHeader)

  plug(Plug.Logger)
  plug(:match)
  plug(:dispatch)

  post "/" do
    attrs = conn.body_params

    case Events.create_event(attrs) do
      {:ok, event} ->
        conn
        |> send_resp(200, message(event))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> send_resp(400, message(errors_on(changeset)))

      {:error, %{error: _msg} = error} ->
        conn
        |> send_resp(400, message(error))
    end
  end

  post "/:id/participant" do
    attrs = conn.body_params

    case Events.add_participant(Map.put(attrs, "id", id)) do
      %EventsApp.Model.Event{} = event ->
        conn
        |> send_resp(200, message(event))

      {:error, %{error: _msg} = error} ->
        conn
        |> send_resp(400, message(error))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> send_resp(400, message(errors_on(changeset)))

      %Ecto.Changeset{} = changeset ->
        conn
        |> send_resp(400, message(errors_on(changeset)))
    end
  end

  get "/:id/participants" do
    case Events.get_all_participants(id) do
      [_ | _] = participants ->
        conn
        |> send_resp(200, message(%{participants: participants}))

      [] ->
        conn
        |> send_resp(200, message(%{participants: []}))

      {:error, error} ->
        conn
        |> send_resp(400, message(error))
    end
  end

  delete "/:id/participants/:participant_id" do
    case Events.remove_participant(id, participant_id) do
      %EventsApp.Model.Event{} = event ->
        conn
        |> send_resp(200, message(event))

      {:error, %{error: _msg} = error} ->
        conn
        |> send_resp(400, message(error))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> send_resp(400, message(errors_on(changeset)))
    end
  end

  delete "/:id" do
    case Events.delete_event(id) do
      %EventsApp.Model.Event{} = event ->
        conn
        |> send_resp(200, message(event))

      {:error, %{error: _msg} = error} ->
        conn
        |> send_resp(400, message(error))

      %Ecto.Changeset{} = changeset ->
        conn
        |> send_resp(400, message(errors_on(changeset)))
    end
  end

  put "/:id" do
    attrs = conn.body_params

    case Events.update_event(id, attrs) do
      %EventsApp.Model.Event{} = event ->
        conn
        |> send_resp(200, message(event))

      {:error, %{error: _msg} = error} ->
        conn
        |> send_resp(400, message(error))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> send_resp(400, message(errors_on(changeset)))

      %Ecto.Changeset{} = changeset ->
        conn
        |> send_resp(400, message(errors_on(changeset)))
    end
  end

  get "/" do
    %Scrivener.Page{entries: result} = Events.get_events_by(conn.params)
    send_result(result, conn)
  end

  match _ do
    send_resp(conn, 404, "Requested page not found!")
  end

  defp send_result(result, conn) do
    case result do
      [%EventsApp.Model.Event{} | _rest] = events ->
        conn
        |> send_resp(200, message(events))

      [] ->
        conn
        |> send_resp(200, message([]))

      {:error, error} ->
        conn
        |> send_resp(400, message(error))
    end
  end

  @doc """
  A helper that transforms changeset errors into a map of messages.

      assert {:error, changeset} = Accounts.create_user(%{password: "short"})
      assert "password is too short" in errors_on(changeset).password
      assert %{password: ["password is too short"]} = errors_on(changeset)

  """
  def errors_on(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Enum.reduce(opts, message, fn {key, value}, acc ->
        try do
          String.replace(acc, "%{#{key}}", to_string(value))
        rescue
          _e in Protocol.UndefinedError ->
            new_value = value |> Tuple.to_list() |> Enum.map(fn el -> to_string(el) end)
            String.replace(acc, "%{#{key}}", "#{new_value}")
        end
      end)
    end)
  end

  defp message(object) do
    Jason.encode!(object)
  end
end
