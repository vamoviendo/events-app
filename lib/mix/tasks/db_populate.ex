defmodule Mix.Tasks.Events.Populate do
  use Mix.Task
  import Jason
  alias EventsApp.Repo
  alias EventsApp.Query.Events
  alias EventsApp.Model.Event


  @shortdoc "Given a JSON file, populates database"

  @switches [file: :string]

  @aliases [f: :file]

  @moduledoc """
  This task populates a DB with 1000 records
  This is all fake data.

  ## Examples

      mix users.populate # Asumes JSON file is on it path
      mix users.populate -f /path/to/json_file

  ## Command line options

    * `-f`, `--file` - the path to the JSON file
  """

  @doc false
  def run(args) do
    Application.ensure_all_started(:events_app)

    {opts, _} = OptionParser.parse! args, strict: @switches, aliases: @aliases

    file_path = opts[:file] || Path.wildcard("./*.json")

    {:ok, json} = with {:ok, body} <- File.read(file_path),
                       {:ok, json} <- Jason.decode(body), do: {:ok, json}

    for entry <- json do
      case Events.create_event(entry) do
        %Event{} = event ->
          Mix.shell.info "Event has created: #{inspect(event)}..."

        {:ok, event} ->
          Mix.shell.info "Event has created: #{inspect(event)}..."

        {:error, %Ecto.Changeset{} = changeset} ->
          Mix.shell.error "Problems Creating event: #{inspect(changeset.errors)}..."

        {:error, error} ->
          Mix.shell.error "Problems Creating event: #{inspect(error)}..."
      end
    end
  end
end
