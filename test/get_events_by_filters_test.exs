defmodule EventsApp.Router.FilterEvents.Test do
  use EventsApp.TestCase
  alias EventsApp.Test.Helpers

  setup do
    events = Helpers.seed_with_new_events()

    on_exit(fn ->
      Repo.delete_all(Event)
    end)

    events
  end

  describe "#GET /events?id=1" do
    test "returns only the event with given id" do
      id = Helpers.get_id_from_list()
      {_, conn, result} = send(:get, "/", creator_id: "10000001", id: id)

      assert conn.state == :sent
      assert conn.status == 200
      assert Kernel.length(result) == 1
    end
  end

  describe "#GET /events?exclude_creator_ids=10_000_001" do
    test "returns only the events where creator_id is NOT the given one" do
      {_, _, result} = send(:get, "/", exclude_creator_ids: "10000001")

      assert 10_000_001 not in Enum.map(result, fn item ->
               item["creator_id"]
             end)
    end
  end
end
