defmodule EventsApp.Router.DeleteEvents.Test do
  use EventsApp.TestCase
  alias EventsApp.Test.Helpers

  def set_to_deleted(id) do
    send(:delete, "/#{id}", nil)
  end

  setup_all do
    events = Helpers.seed_with_new_events()

    on_exit(fn ->
      Repo.delete_all(Event)
    end)

    events
  end

  describe "#Delete /:id" do
    test "can not be deleted if :deleted set to true" do
      id = Helpers.get_id_from_list()
      set_to_deleted(id)

      {_, conn, result} = send(:delete, "/#{id}", nil)

      assert conn.status == 400
      assert ["Event can not be modified"] = result["error"]
    end

    test "on GET events, deleted ones are not shown" do
      id = Helpers.get_id_from_list()
      set_to_deleted(id)

      {_, conn, result} = send(:get, "/", id: id)

      assert conn.status == 200
      assert [] = result
    end
  end
end
