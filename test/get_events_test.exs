defmodule EventsApp.Router.GetAllEvents.Test do
  use EventsApp.TestCase
  alias EventsApp.Test.Helpers

  setup_all do
    events = Helpers.seed_with_new_events()

    on_exit(fn ->
      Repo.delete_all(Event)
    end)

    events
  end

  describe "#GET /events" do
    test "returns all the exiting events" do
      # Create a test connection
      {_, conn, result} = send(:get, "/", nil)

      assert conn.state == :sent
      assert conn.status == 200
      assert Kernel.length(result) == 10
    end
  end
end
