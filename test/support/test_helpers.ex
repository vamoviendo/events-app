defmodule EventsApp.Test.Helpers do
  alias EventsApp.Query.Events
  use Plug.Test

  def seed_with_new_events do
    create_event = fn id ->
      Events.create_event(%{
        creator_id: Integer.to_string(id),
        name: "foo",
        participants: ["1", "2", "3"]
      })
    end

    events = for num <- Enum.to_list(10_000_001..10_000_010), do: create_event.(num)
    {:ok, event: events}
  end

  def get_id_from_list do
    %Scrivener.Page{entries: result} = Events.list_all_events()
    [%EventsApp.Model.Event{} = event | _] = result
    event.id
  end
end
