defmodule EventsApp.RepoCase do
  use ExUnit.CaseTemplate
  alias EventsApp.Repo
  alias Ecto.Adapters.SQL.Sandbox

  using do
    quote do
      import EventsApp.RepoCase
      alias EventsApp.Repo
      import Ecto
      import Ecto.Query
    end
  end

  setup tags do
    :ok = Sandbox.checkout(Repo)

    unless tags[:async] do
      Sandbox.mode(Repo, :auto)
    end

    :ok
  end
end
