defmodule EventsApp.TestCase do
  use ExUnit.CaseTemplate
  use Plug.Test

  defmacro __using__(_) do
    quote do
      # This code is injected into every case that calls "use MyCase"
      use EventsApp.RepoCase
      use ExUnit.Case, async: false

      alias EventsApp.Query.Events
      alias EventsApp.Model.Event

      @opts EventsApp.Router.init([])

      import unquote(__MODULE__)

      defdelegate send, to: EventsApp.TestCase
    end
  end

  def send(method, path, params, decode \\ true) do
    conn =
      conn(method, path, params)
      |> put_req_header("content-type", "application/json")
      |> EventsApp.Router.call(@opts)

    if decode do
      result = Jason.decode!(conn.resp_body)
      {:ok, conn, result}
    else
      {:ok, conn}
    end
  end
end
