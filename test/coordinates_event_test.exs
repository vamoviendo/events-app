defmodule EventsApp.Router.Coordinates.Test do
  use EventsApp.TestCase
  alias EventsApp.Test.Helpers

  @coordinates %{lat: "-123123123123", lon: "345345345345"}
  @params %{
    name: "grey_geo",
    creator_id: "24",
    coordinates: @coordinates
  }

  setup_all do
    events = Helpers.seed_with_new_events()

    on_exit(fn ->
      Repo.delete_all(Event)
    end)

    events
  end

  describe "#POST /events with coordinates" do
    test "creates an event with coordinates" do
      {_, _, result} = send(:post, "/", @params)

      assert %{"lat" => _, "lon" => _} = result["coordinates"]
    end
  end

  describe "#POST /events with wrong coordinates" do
    test "coordinates are not accepted" do
      coordinates = "{-123123123123,345345345345}"

      wrong_params = %{
        name: "grey_geo",
        creator_id: "24",
        coordinates: coordinates
      }

      {_, _, result} = send(:post, "/", wrong_params)

      assert %{"coordinates" => ["is invalid"]} = result
    end
  end
end
