defmodule EventsApp.Router.AddressField.Test do
  use EventsApp.TestCase

  @params %{
    name: "grey_geo",
    creator_id: "24",
    address: "nepper 1143, 08003"
  }

  @params_with_address_type_wrong %{
    name: "grey_geo",
    creator_id: "24",
    address: ["nepper 1143, 08003"]
  }

  describe "#POST Creates an event with address field" do
    test "then new event has address" do
      {_, _, result} = send(:post, "/", @params)

      assert result["address"] == @params[:address]
    end
  end

  describe "#POST When address type is not valid" do
    test "then field error is displayed" do
      {_, _, result} = send(:post, "/", @params_with_address_type_wrong)

      assert result["address"] == ["is invalid"]
    end
  end
end
