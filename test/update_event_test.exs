defmodule EventsApp.Router.UpdateEvent.Test do
  use EventsApp.TestCase
  alias EventsApp.Test.Helpers

  @wrong_coordinates %{coordinates: %{foo: "bar"}}

  @string_params %{
    name: "changed_name",
    creator_id: "55555",
    type: "food",
    status: "created",
    starts_at: "2019-06-17 20:49:00.000"
  }

  setup_all do
    events = Helpers.seed_with_new_events()

    on_exit(fn ->
      Repo.delete_all(Event)
    end)

    events
  end

  describe "#PUT Success" do
    test "update string fileds" do
      id = Helpers.get_id_from_list()

      {_, _, response} = send(:put, "/#{id}", @string_params)

      assert @string_params[:name] == response["name"]
      assert @string_params[:status] == response["status"]
      assert @string_params[:type] == response["type"]
      assert @string_params[:creator_id] == response["creator_id"]
      assert "2019-06-17T20:49:00Z" == response["starts_at"]
    end
  end

  describe "#PUT Failed" do
    test "update event with wrong coordinates" do
      id = Helpers.get_id_from_list()
      {_, _, response} = send(:put, "/#{id}", @wrong_coordinates)

      assert %{"coordinates" => ["is invalid"]} = response
    end
  end
end
