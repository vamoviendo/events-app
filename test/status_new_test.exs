defmodule EventsApp.Router.StatusToNew.Test do
  use EventsApp.TestCase
  alias EventsApp.Test.Helpers

  @params %{
    name: "grey_geo",
    creator_id: "24",
    address: "nepper 1143, 08003"
  }

  setup_all do
    events = Helpers.seed_with_new_events()

    on_exit(fn ->
      Repo.delete_all(Event)
    end)

    events
  end

  describe "#POST Creates an event with status new" do
    test "new events if success should have status to new" do
      {_, _, result} = send(:post, "/", @params)

      assert result["status"] == "new"
    end
  end

  describe "updating an event should not change the actual status" do
    test "edited event keeps actual state" do
      id = Helpers.get_id_from_list()
      {_, _, result} = send(:put, "/#{id}", @params)

      assert result["status"] == "new"
    end
  end
end
