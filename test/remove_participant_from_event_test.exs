defmodule EventsApp.Router.RemoveParticipantsFromEvent.Test do
  use EventsApp.TestCase
  alias EventsApp.Test.Helpers

  setup_all do
    events = Helpers.seed_with_new_events()

    on_exit(fn ->
      Repo.delete_all(Event)
    end)

    events
  end

  describe "#DELETE /events/:id/participants/:participant_id" do
    test "deletes a participant from a given event" do
      id = Helpers.get_id_from_list()
      {_, conn, result} = send(:delete, "/#{id}/participants/1", nil)

      assert conn.status == 200
      assert ["2", "3"] = result["participants"]
    end
  end
end
