defmodule EventsApp.Router.AddParticipants.Test do
  use EventsApp.TestCase
  alias EventsApp.Test.Helpers

  setup_all do
    events = Helpers.seed_with_new_events()

    on_exit(fn ->
      Repo.delete_all(Event)
    end)

    events
  end

  describe "#POST /:id/participant" do
    test "adds a new participant ID to list :participants" do
      id = Helpers.get_id_from_list()
      {_, _, result} = send(:post, "/#{id}/participant", %{"participant_id" => "10000001"})

      assert "10000001" in result["participants"]
    end
  end

  describe "#Failed #POST /:id/participant" do
    test "when :id do not exist returns correct error" do
      # Create a test connection
      {_, conn, result} = send(:post, "/-1/participant", %{"participant_id" => "10000001"})

      assert conn.status == 400
      assert %{"error" => "event not found"} = result
    end
  end

  describe "#Failed #POST" do
    test "when :participant_id is not correct it throws the correct error message" do
      id = Helpers.get_id_from_list()
      {_, conn, result} = send(:post, "/#{id}/participant", %{"participant_id" => 1})

      assert conn.status == 400
      assert %{"participants" => ["is invalid"]} = result
    end
  end

  describe "GET /events/:id/participants" do
    test "returns a list of actual participants" do
      id = Helpers.get_id_from_list()
      {_, conn, result} = send(:get, "/#{id}/participants", nil)

      assert conn.status == 200
      assert %{"participants" => ["1", "2", "3"]} = result
    end
  end

  describe "#Failed #GET /events/:id/participants" do
    test "returns message error when id do not exist" do
      {_, conn, result} = send(:post, "/-1/participant", %{"participant_id" => "10000001"})

      assert conn.status == 400
      assert %{"error" => "event not found"} = result
    end
  end
end
